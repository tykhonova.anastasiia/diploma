<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\{Response,Request};
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class UserController extends AbstractController
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {

    }

    #[Route('/user/view/{id}')]
    public function userView(EntityManagerInterface $entityManager, int $id): Response
    {
        $user = $entityManager->getRepository(User::class)->find($id);

        return $this->render('user.html.twig', ['user' => $user]);
    }

    #[Route('/user/edit/{id}')]
    public function userEdit(Request $request, EntityManagerInterface $entityManager, int $id): Response
    {
        $user = $entityManager->getRepository(User::class)->find($id);
        $message = '';

        if(!empty($_POST) && $user) {
            $entityManager->persist($user);
            $user->setIsEncrypted((int)$request->request->get('is_encrypted'));
            $user->setName($request->request->get('name'));
            $user->setLastname($request->request->get('lastname'));
            $user->setEmail($request->request->get('email'));
            $entityManager->flush();
            $message = 'Saved!';
        }

        return $this->render('user.edit.html.twig', ['user' => $user, 'message' => $message]);
    }

    #[Route('/')]
    public function userSearch(Request $request, EntityManagerInterface $entityManager): Response
    {
        $key = $request->request->get('key');

        $users = $key ? $entityManager->getRepository(User::class)->findByKey($key) : [];

        return $this->render('user.search.html.twig', ['key' => $key, 'users' => $users]);
    }
}