<?php

namespace App\Entity;

use App\Service\EncryptionService;
use App\Service\UserPasswordEncoderInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: "user", options: ["engine" => "MyISAM", "collate" => "utf8mb4_general_ci", "charset" => "utf8mb4"])]
#[ORM\Entity(repositoryClass: "App\Repository\UserRepository")]
class User extends EncryptionService
{

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    #[ORM\Column(name: "id", type: "integer", nullable: true, options: ["default" => "NULL"])]
    private ?int $id;

    #[ORM\Column(name: "is_encrypted", type: "integer", options: ["default" => 0])]
    protected ?int $isEncrypted = 0;

    #[ORM\Column(name: "password", type: "string", length: 400, nullable: true, options: ["default" => "NULL"])]
    private ?string $password = '';

    #[ORM\Column(name: "EMAIL", type: "string", length: 400, nullable: true, options: ["default" => "NULL"])]
    private ?string $email = '';

    #[ORM\Column(name: "FIRSTNAME", type: "string", length: 400, nullable: true, options: ["default" => "NULL"])]
    private ?string $name = '';

    #[ORM\Column(name: "LASTNAME", type: "string", length: 400, nullable: true, options: ["default" => "NULL"])]
    private ?string $lastname = '';


    /**
     * @param string $password
     */
    public function setPassword(?string $password, UserPasswordHasherInterface $passwordHasher): void
    {
        if (is_null($password)) {
            $this->password = null;
        } else {
            $this->password = $passwordHasher->hash($this, $password);
        }

    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIsEncrypted(): ?int
    {
        return $this->isEncrypted;
    }

    /**
     * @param int|null $isEncrypted
     */
    public function setIsEncrypted(?int $isEncrypted): void
    {
        $this->isEncrypted = $isEncrypted;
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->decrypt($this->email);
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $this->encrypt($email);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->decrypt($this->name);
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $this->encrypt($name);
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->decrypt($this->lastname);
    }

    /**
     * @param string|null $lastname
     */
    public function setLastname(?string $lastname): void
    {
        $this->lastname = $this->encrypt($lastname);
    }

}