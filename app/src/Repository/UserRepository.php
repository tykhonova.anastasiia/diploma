<?php

namespace App\Repository;

use App\Entity\User;
use App\Service\EncryptionService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class UserRepository extends ServiceEntityRepository
{

    public function __construct(
        ManagerRegistry                         $registry,
        private readonly EntityManagerInterface $entityManager,
        private readonly EncryptionService      $encryptionService
    )
    {
        parent::__construct($registry, User::class);
    }

    public function findByEmail(string $email): ?User
    {
        return $this->findOneBy(['email' => $email]);
    }


    public function findByKey(string $key): array
    {
        $encryptedKey = $this->encryptionService->encrypt($key, true);

        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.email LIKE :key')
            ->orWhere('u.email LIKE :encrypted_key')
            ->orWhere('u.name LIKE :key')
            ->orWhere('u.name LIKE :encrypted_key')
            ->orWhere('u.lastname LIKE :key')
            ->orWhere('u.lastname LIKE :encrypted_key')
            ->setParameter('key', '%' . $key . '%')
            ->setParameter('encrypted_key', '%' . $encryptedKey . '%')
            ->getQuery()
            ->getResult();
    }
}