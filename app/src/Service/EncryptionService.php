<?php

namespace App\Service;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\NativePasswordEncoder;

class EncryptionService
{
    protected ?int $isEncrypted = 0;

    public function encrypt(string $data, ?bool $encrypt = false): string
    {
        if(!$this->isEncrypted && !$encrypt) {
            return $data;
        }

        $array = explode(' ', $data);
        $result = [];
        foreach ($array as $item) {
            $result[] = $this->encryptString($item);
        }
        return implode(' ', $result);
    }

    public function decrypt(string $data, ?bool $encrypt = false): string
    {
        if(!$this->isEncrypted && !$encrypt) {
            return $data;
        }

        $array = explode(' ', $data);
        $result = [];
        foreach ($array as $item) {
            $result[] = $this->decryptString($item);
        }
        return implode(' ', $result);
    }

    private function decryptString(string $data): string
    {
        return $data ? openssl_decrypt($data, 'AES-256-CBC', $_ENV['KEY'], 0, substr($_ENV['KEY'], 0, 16)) : '';
    }

    private function encryptString(string $data): string
    {
        return $data ? openssl_encrypt($data, 'AES-256-CBC', $_ENV['KEY'], 0, substr($_ENV['KEY'], 0, 16)) : '';
    }
}
