<?php

namespace App\Tests\Functional\Users\Application\Query\FindUserByEmail;

use App\Shared\Application\Query\QueryBusInterface;
use App\Tests\Resource\Fixture\UserFixture;
use App\Users\Application\DTO\UserDTO;
use App\Users\Application\Query\FindUserByEmail\FindUserByEmailQuery;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use App\Tests\Tools\FakerTools;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FindUserByEmailQueryHandlerTest extends WebTestCase
{
    use FakerTools;

    private ?object $queryBus;
    private $userRepository;

    private $databaseTool;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->queryBus = $this::getContainer()->get(QueryBusInterface::class);
        $this->userRepository = $this::getContainer()->get(DatabaseToolCollection::class)->get();
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
    }

    public function test_user_created_when_commnad_executed(): void {
        $referenceRepository = $this->databaseTool->loadFixtures([UserFixture::class])->getReferenceRepository();

        $user = $referenceRepository->getReference(UserFixture::REFERENCE);
        $query = new FindUserByEmailQuery($user->getEmail());

        $userDTO = $this->queryBus->execute($query);

        $this->assertInstanceOf(UserDTO::class, $userDTO);
    }
}
