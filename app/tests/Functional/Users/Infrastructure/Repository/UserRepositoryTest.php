<?php

namespace App\Tests\Functional\Users\Infrastructure\Repository;

use App\Tests\Resource\Fixture\UserFixture;
use App\Tests\Tools\FakerTools;
use App\Users\Domain\Factory\UserFactory;
use App\Users\Infrastructure\Repository\UserRepository;
use Faker\Generator;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRepositoryTest extends WebTestCase
{
    use FakerTools;

    private UserRepository $repository;
    private Generator $faker;
    private UserFactory $userFactory;
    private AbstractDatabaseTool $databaseTool;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = static::getContainer()->get(UserRepository::class);
        $this->userFactory = static::getContainer()->get(UserFactory::class);
        $this->faker = $this->getFaker();
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();

    }

    public function test_user_added_successfuly(): void
    {
        $email = $this->faker->email();
        $password = $this->faker->password();
        $user = $this->userFactory->create($email, $password);

        $this->repository->add($user);

        $existedUser = $this->repository->findByUlid($user->getUlid());

        $this->assertEquals($user->getUlid(), $existedUser->getUlid());
    }

    public function test_user_found_successfully(): void
    {
        $executor = $this->databaseTool->loadFixtures([UserFixture::class]);
        $user = $executor->getReferenceRepository()->getReference(UserFixture::REFERENCE);
        $existedUser = $this->repository->findByUlid($user->getUlid());
        $this->assertEquals($user->getUlid(), $existedUser->getUlid());
    }
}
